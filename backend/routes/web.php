<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use Firebase\JWT\JWT;  

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/list_booking', function(){
	$results = app('db')->select("SELECT * FROM booking");
	return response()->json($results);
});
$router->post('/add_booking', function(Illuminate\Http\Request $request) {
	
	$fieldid = $request->input("fieldid");
	$enddate = $request->input("enddate");
	
	$startdate = date( 'Y-m-d ');

	$query = app('db')->insert('INSERT into booking
									(startdate, enddate, fieldid)
									VALUES(?, ?, ?)',
									[
									  $startdate,
									  $enddate,
									  $fieldid ]);
	return "Ok";

});

$router->delete('/delete_booking', ['middleware'=>'auth',  function(Illuminate\Http\Request $request){
	$bookingid = $request->input("bookingid");
	$query = app('db')->delete('DELETE FROM booking
								WHERE
									bookingid=?',
									[$bookingid]);
		return "Ok";
}]);
$router->put('/update_booking', function(Illuminate\Http\Request $request){
	
	$enddate = $request->input("enddate");
	
	$query = app('db')->update('UPDATE booking set enddate = ?',[$enddate]);
	if ($query)
		return response()->json(["STATUS" => "SUCCESS"]);
});
$router->post('/login', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $password = $request->input("password");
    $result = app('db')->select(
        'SELECT userid,password from user WHERE username = ?',
        [$username]
    );
    if ($result == null) {
        return response()->json(["STATUS" => "0"]);
    }
    $loginResult = new stdClass();
    if (count($result) == 0) {
        $loginResult->STATUS = "fail";
        $loginResult->reason = "User is not founded";
    } else {
        if (app('hash')->check($password, $result[0]->password)) {
            $loginResult->STATUS = "SUCCESS";
            $payload = [
                'iss' => "booking_bizzmore",
                'sub' => $result[0]->userid,
                'iat' => time(),
                'exp' => time() + 30 * 60 * 60,
            ];
            $loginResult->token = JWT::encode($payload, env('APP_KEY'));
            return response()->json($loginResult);
        } else {
            $loginResult->STATUS = "fail";
            $loginResult->reason = "Incorrect Password";
            return response()->json($loginResult);
        }
    }
    return response()->json($loginResult);
});


$router->post('/register', function (\Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $query = app('db')->select("SELECT username FROM user WHERE username = ?", [$username]);
    if (count($query) > 0) {
        return response()->json(["STATUS" => "username is USED"]);
    }
    $password = $request->input("password");
    $repassword = $request->input("repassword");
    $firstname = $request->input("firstname");
    $lastname = $request->input("lastname");
    if (strcmp($password, $repassword) == 0) {
        $password = app('hash')->make($password);
        $query = app('db')->insert(
            'INSERT INTO user(username,password,firstname,lastname) VALUES (?,?,?,?)',
            [$username, $password, $firstname, $lastname]
        );
        return response()->json(["STATUS" => "SUCCESS"]);
    }
    return response()->json(["STATUS" => "FAIL"]);
});